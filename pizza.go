package main

import (
	"bufio"
	"fmt"
	"os"
	"sort"
	"strconv"
	"strings"
)

var inputFile = "data/input_set_a.txt"

func main() {
	if len(os.Args) > 1 {
		inputFile = os.Args[1]
	}

	file, err := os.Open(inputFile)
	if err != nil {
		panic(err)
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)

	// Handle first line
	scanner.Scan()
	line := strings.Split(scanner.Text(), " ")
	maxSlices, _ := strconv.Atoi(line[0])
	numTypes, _ := strconv.Atoi(line[1])
	var pickedTypes []int

	// Handle the second line
	scanner.Scan()
	types := strings.Split(scanner.Text(), " ")
	for x := numTypes - 1; x >= 0; x-- {
		currentType, _ := strconv.Atoi(types[x])
		if currentType > -1 && currentType < maxSlices {
			pickedTypes = append(pickedTypes, x)
			maxSlices = maxSlices - currentType
			types[x] = "-1"
		}
	}
	sort.Ints(pickedTypes)
	fmt.Printf("%v \n", len(pickedTypes))
	for _, pizzaType := range pickedTypes {
		fmt.Printf("%v ", pizzaType)
	}
}
