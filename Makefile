submit:
	# rm result/pizza.zip
	zip result/pizza.zip pizza.go
	go run pizza.go data/input_set_a.txt > result/a.txt
	go run pizza.go data/input_set_b.txt > result/b.txt
	go run pizza.go data/input_set_c.txt > result/c.txt
	go run pizza.go data/input_set_d.txt > result/d.txt
	go run pizza.go data/input_set_e.txt > result/e.txt