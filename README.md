# HashCode 2020 practice round

A quite nice Problem where it is really easy to get firs results, but I guess this can be optimized to get a few more thousand points in total.

# Solution - SPOILER - 

My solution is fairly simple, but thus not perfect:

I iterate in reverse over the slices ans search for the next biggest slice that would fit into my max slices. This is then substracted from my total. So on and so forth...
Works like a charm and brings me a ton of points.

## Optimizations

Some of the datasets have a quite big minimum slice size. If the smallest slice is 250, but I have 249 slices remaining, then I'm screwed.
This could probably be solved by not always using the largest, but by splitting this a bit and by finding a way to use the best combination possible to fill all slices.... 